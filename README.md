Portal Faker
============

`portal-faker` is a commandline utility to fake the DBus API(s) of XDG desktop
portals. This is a debugging tool for a rather niche part of the desktop stack,
so expect the UI to be a bit rough around the edges.

Currently this tool needs to be run from within the git repository.

```
$ ./portal-faker --help
$ ./portal-faker list-portals
```

As a rule of thumb, the CLI looks like one of the following commands,
depending whether you want to fake the `org.freedestkop.portal` or the
`org.freedesktop.impl.portal` of the portal:

```
$ portal-faker portal PortalName MethodName --foo --bar arg1 arg2
$ portal-faker impl PortalName MethodName arg1 arg2
```

Portals are implemented on an as-needed basis, don't expect your favorite
portal to be present.

## Code Layout

- `portalfaker/__init__.py` all core functionality
- `portalfaker/cli/` *generic* CLI functionality
- `portalfaker/foo.py` *logic* for the `Foo` DBus portal
- `portalfaker/portal/__init__.py` DBus interfaces for `org.freedesktop.portal.*`
- `portalfaker/impl/portal/__init__.py` DBus interfaces for `org.freedesktop.impl.portal.*`

The DBus interfaces are wrappers to/fro the DBus API and should be mostly logic-free.
