#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

# Implementation of all org.freedesktop.portal.* interfaces

from portalfaker import DBusObject, Response, Reply, RequestHandle
from typing import Any, Dict
from gi.repository import Gio, GLib, GObject  # type: ignore

import attr
import logging
import portalfaker.inputcapture

logger = logging.getLogger("portalfaker.portal")


@attr.s
class Request(GObject.Object):
    """
    Implementation of the org.freedesktop.portal.Request DBus interface. See
    the portal documentation for details but basically:

    Some client calls a portal DBus method with a request "handle_token".
    From that we create an object path and create the Request DBus object.
    The caller *may* Close the request for any reason, or we may send out the
    Closed signal for any reason to interrupt.

    If all works well, send the data via the Response signal.
    """

    objpath: str = attr.ib()
    _dbus_obj: DBusObject = attr.ib()
    sender: str = attr.ib()

    __gsignals__ = {
        "closed": (GObject.SignalFlags.RUN_FIRST, None, ()),
    }

    def __attrs_pre_init__(self):
        super().__init__()

    @property
    def handle(self) -> RequestHandle:
        return RequestHandle(self.objpath)

    def close(self):
        """
        Entry point for our part of the portal to close the request. This does
        **not** remove the request.
        """
        self.signal("Closed")  # this is the dbus signal

    def Close(self) -> None:
        """
        DBus entry point when the request is closed by the caller.
        """
        self.emit("closed")  # this is the gobject signal

    def respond(self, response: Response):
        self._dbus_obj.signal("Response", response.value, dest=self.sender)
        self.remove()

    def remove(self) -> None:
        self._dbus_obj.remove()

    @classmethod
    def create(cls, connection, sender: str, handle_token: str):
        sender_token = sender.removeprefix(":").replace(".", "_")
        objpath = (
            f"/org/freedesktop/portal/desktop/request/{sender_token}/{handle_token}"
        )

        dbus_obj = DBusObject.create_from_interface_name(
            connection=connection,
            interface="org.freedesktop.portal.Request",
            objpath=objpath,
            wrapper=None,
        )
        c = cls(objpath=objpath, dbus_obj=dbus_obj, sender=sender)
        dbus_obj.register()
        return c


@attr.s
class Session(GObject.Object):
    """
    Implementation of the org.freedesktop.portal.Session DBus interface. See
    the portal documentation for details but basically:

    Some client calls a CreateSession DBus method with a session handle
    That becomes our object path for the session DBus object. (Note: this
    differs to the portal.Session implementation.)

    The xdg-desktop-portal *may* Close the session for any reason, or we may
    send out the Closed signal for any reason to interrupt.
    """

    handle: str = attr.ib()
    _dbus_obj: DBusObject = attr.ib()

    __gsignals__ = {
        "closed": (GObject.SignalFlags.RUN_FIRST, None, ()),
    }

    def __attrs_pre_init__(self):
        super().__init__()

    def close(self):
        """
        Entry point for the impl.portal to close the session. This does **not**
        remove the session.
        """
        self.signal("Closed")  # this is the dbus signal

    def Close(self) -> None:
        """
        DBus entry point when the session is closed by the xdg-desktop-portal.
        Emits the "closed" GObject signal.
        """
        self.emit("closed")  # this is the gobject signal

    def remove(self) -> None:
        self._dbus_obj.remove()

    @classmethod
    def create(cls, connection, sender: str, session_token: str):
        """
        Create a Session on DBus for the given sender with the given sender token.
        """
        sender_token = sender.removeprefix(":").replace(".", "_")
        objpath = (
            f"/org/freedesktop/portal/desktop/session/{sender_token}/{session_token}"
        )

        dbus_obj = DBusObject.create_from_interface_name(
            connection=connection,
            interface="org.freedesktop.portal.Session",
            objpath=objpath,
            wrapper=None,
        )
        c = cls(handle=objpath, dbus_obj=dbus_obj)
        dbus_obj.register()
        return c


@attr.s
class InputCapture:
    """
    Implementation of the org.freedesktop.portal.InputCapture DBus interface.
    See the portal documentation for details.

    See the portalfaker.inputcapture module for the actual logic, this is
    merely the wrapper class to/fro the DBus interface.
    """

    connection: Gio.DBusConnection = attr.ib()
    _dbus_obj: DBusObject = attr.ib()
    config: Dict[str, Any] = attr.ib(factory=dict)
    sessions: Dict[str, Session] = attr.ib(factory=dict, init=False)

    @classmethod
    def create(cls, dbus_obj: DBusObject, config=None):
        instance = cls(
            connection=dbus_obj.connection, dbus_obj=dbus_obj, config=config or {}
        )
        dbus_obj.wrapper = instance
        return instance

    def CreateSession(self, parent_window, options, metainfo) -> RequestHandle:
        caps = options["capabilities"]
        handle_token = options["handle_token"]
        session_handle_token = options["session_handle_token"]

        dbus_session = Session.create(
            self.connection, metainfo["sender"], session_handle_token
        )
        session = portalfaker.inputcapture.Session.create(
            self.connection, dbus_session, caps, self.config
        )
        self.sessions[session.handle] = session

        request = Request.create(self.connection, metainfo["sender"], handle_token)

        # Session signals
        def Disabled(session):
            self._dbus_obj.signal(
                "Disabled",
                GLib.Variant("(oa{sv})", (session.handle, {})),
                dest=metainfo["sender"],
            )

        def Activated(session, opts):
            options = {
                "activation_id": GLib.Variant("u", opts["activation-id"]),
                "cursor_position": GLib.Variant("(dd)", opts["cursor-position"]),
                "barrier_id": GLib.Variant("u", opts["barrier-id"]),
            }
            self._dbus_obj.signal(
                "Activated",
                GLib.Variant("(oa{sv})", (session.handle, options)),
                dest=metainfo["sender"],
            )

        def Deactivated(session, opts):
            options = {
                "activation_id": GLib.Variant("u", opts["activation-id"]),
            }
            self._dbus_obj.signal(
                "Deactivated",
                GLib.Variant("(oa{sv})", (session.handle, options)),
                dest=metainfo["sender"],
            )

        def ZonesChanged(session):
            self._dbus_obj.signal(
                "ZonesChanged",
                GLib.Variant("(oa{sv})", (session.handle, {})),
                dest=metainfo["sender"],
            )

        session.connect("disabled", Disabled)
        session.connect("activated", Activated)
        session.connect("deactivated", Deactivated)
        session.connect("zones-changed", ZonesChanged)

        results = {
            "capabilities": GLib.Variant.new_uint32(session.capabilities),
            "session_handle": GLib.Variant.new_object_path(session.handle),
        }
        request.respond(Response(0, results))
        return request.handle

    def ConnectToEIS(self, session_handle, options) -> GLib.Variant:
        session = self.sessions[session_handle]
        return Reply(
            value=GLib.Variant("(h)", (0,)), fd_list=[session.eis_socket.fileno()]
        )

    def GetZones(self, session_handle, options, metainfo) -> RequestHandle:
        handle_token = options["handle_token"]
        session = self.sessions[session_handle]

        results = {
            "zone_set": GLib.Variant("u", session.zone_set),
            "zones": GLib.Variant(
                "a(uuii)",
                [
                    GLib.Variant("(uuii)", (z.w, z.h, z.x, z.y))
                    for z in session.zones
                    if z.id == session.zone_set
                ],
            ),
        }

        request = Request.create(self.connection, metainfo["sender"], handle_token)
        request.respond(Response(0, results))
        return request.handle

    def SetPointerBarriers(
        self, session_handle, options, barriers, zone_set, metainfo
    ) -> RequestHandle:
        handle_token = options["handle_token"]
        session = self.sessions[session_handle]

        bs = []
        failed = []
        for barrier in barriers:
            id = barrier["barrier_id"]
            position = barrier["position"]

            b = portalfaker.inputcapture.Barrier.create(id, position)
            bs.append(b)

        if bs:
            session.set_barriers(bs, zone_set=zone_set)
            success = [b.id for b in session.barriers]
            failed.extend([b.id for b in bs if b.id not in success])

        response = 0
        results = {"failed_barriers": GLib.Variant("au", failed)}

        request = Request.create(self.connection, metainfo["sender"], handle_token)
        request.respond(Response(0, results))
        return request.handle

    def Enable(self, session_handle, options) -> None:
        session = self.sessions[session_handle]
        session.enable()

    def Disable(self, session_handle, options) -> None:
        session = self.sessions[session_handle]
        session.disable()

    def Release(self, session_handle, options) -> None:
        session = self.sessions[session_handle]
        session.release()

    @property
    def version(self) -> int:
        v = self.config.get("version", 1)
        return GLib.Variant.new_uint32(v)

    @property
    def SupportedCapabilities(self) -> int:
        c = self.config.get("capabilities", 0b111)
        return GLib.Variant.new_uint32(c)
