#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from pathlib import Path
from gi.repository import GLib, GObject, Gio  # type: ignore
from typing import Any, Callable, Dict, List, Optional

import attr
import importlib
import inspect
import logging
import os
import sys

ASVType = Dict[str, Any]

DATADIR = "/usr/share/"

logger = logging.getLogger("portalfaker.core")


def load_introspection(interface: str) -> Gio.DBusInterfaceInfo:
    """
    Load the data/org.freedesktop.*.xml file and return the interface specification

    This defaults to the git repo's data/ directory and falls back to the
    system-installed ones where an interface isn't available.
    """
    dirs = ["data"] + [
        f"{d}/dbus-1/interfaces" for d in os.getenv("XDG_DATA_DIRS", DATADIR).split(":")
    ]
    for dir in dirs:
        xml_file = Path(dir) / f"{interface}.xml"
        if xml_file.exists():
            logger.debug(f"Loading {xml_file}")
            with open(xml_file) as fd:
                introspection = Gio.DBusNodeInfo.new_for_xml(fd.read())
                return introspection.lookup_interface(interface)
    logger.error(f"Unable to file XML interface specification for {interface}")
    logger.error("Try running me from the git root")
    sys.exit(1)


def print_portalfile_override(interface: str):
    """
    Print a helpful portal template file, plus a helpful warning if something already provides our path.
    """
    from configparser import ConfigParser

    busname = None

    # Print who usually provides this interface
    desktop_type = os.getenv("XDG_SESSION_DESKTOP", "gnome")
    data_dirs = os.getenv("XDG_DATA_DIRS", DATADIR).split(":")
    for data_dir in data_dirs:
        portals_dir = Path(data_dir) / "xdg-desktop-portal" / "portals"
        for portal_file in Path(portals_dir).glob("*.portal"):
            config = ConfigParser()
            config.read(portal_file)
            try:
                if config["portal"]["UseIn"] != desktop_type:
                    continue
                interfaces = config["portal"]["Interfaces"].split(";")
                if interface in interfaces:
                    busname = config["portal"]["DBusName"]
                    logger.info(
                        f"Existing portal file {portal_file} provides {busname}"
                    )
                    return
            except KeyError:
                pass

    from string import Template

    template = """
    # Save the following file in XDG_DESKTOP_PORTAL_DIR, usually
    # ${DATADIR}/xdg-desktop-portal/portals
    [portal]
    DBusName=${busname}
    Interfaces=${interface}
    UseIn=${desktop}
    """
    import textwrap

    s = Template(textwrap.dedent(template)).substitute(
        busname=busname,
        interface=interface,
        desktop=desktop_type,
        DATADIR=DATADIR,
    )
    print(str(s))


@attr.s
class Reply:
    """
    Reply from a normal method call in a DBus implementation.
    """

    value: GLib.Variant = attr.ib()
    fd_list: List[int] = attr.ib(factory=list)


@attr.s
class Response:
    """
    Reponse from a request-based method call in a DBus implementation
    """

    response: int = attr.ib()
    results: ASVType = attr.ib()
    fd_list: List[int] = attr.ib(factory=list)

    @property
    def value(self):
        return GLib.Variant("(ua{sv})", (self.response, self.results))


@attr.s
class RequestHandle:
    handle: GLib.Variant = attr.ib()

    @property
    def value(self):
        return GLib.Variant("(o)", (self.handle,))

    @property
    def fd_list(self):
        return []  # for compat with Reply/Request


@attr.s
class DBusServer(GObject.Object):
    """
    Helper class to just manage the daemon part of the DBus service.

        >>> s = DBusServer.create("org.foo.bar")
        >>> s.connect("bus-name-acquired", do_something)
        >>> s.connect("bus-name-lost", do_something_else)
        >>> s.try_own_name()

    This object sends a signal when the bus name was acquired
    """

    busname: str = attr.ib()
    connection: Gio.DBusConnection = attr.ib(init=False)

    __gsignals__ = {
        "bus-name-acquired": (GObject.SignalFlags.RUN_FIRST, None, ()),
        "bus-name-lost": (GObject.SignalFlags.RUN_FIRST, None, ()),
    }

    def __attrs_pre_init__(self):
        super().__init__()

    def try_own_name(self):
        def bus_acquired(connection, name):
            self.connection = connection

        def bus_name_acquired(connection, name):
            logger.debug(f"Acquired DBus name {name}")
            self.emit("bus-name-acquired")

        def bus_name_lost(connection, name):
            logger.debug(f"Lost DBus name {name}")
            self.emit("bus-name-lost")

        self._dbus = Gio.bus_own_name(
            Gio.BusType.SESSION,
            self.busname,
            Gio.BusNameOwnerFlags.NONE,
            bus_acquired,
            bus_name_acquired,
            bus_name_lost,
        )

        return self

    @classmethod
    def create(cls, busname: str):
        return cls(busname)


@attr.s
class DBusObject(GObject.Object):
    """
    Wrapper around a DBus object. The wrapper object must have the DBus methods
    with the equivalent name, for example let's assume there's some echo interface with
    an Echo function:

    >>> class Foo:
    ...   def Echo(self, message: str):
    ...        return Reply(GLib.Variant("s", message))
    >>>
    >>> o = DBusObject.create_from_interface_name(conn, iface, objpath, wrapper=Foo())
    >>> o.register()

    Whenever ``Echo`` is called on the DBus object, ``Foo.Echo`` is invoked on our object.

    """

    connection: Gio.DBusConnection = attr.ib()
    objpath: str = attr.ib()
    _interface_info: Gio.DBusInterfaceInfo = attr.ib()
    wrapper: Optional[Any] = attr.ib(default=None)
    _dbus_obj: Optional[int] = attr.ib(init=False, default=None)

    def __attrs_pre_init__(self):
        super().__init__()

    def register(self) -> "DBusObject":
        assert self._dbus_obj is None
        self._dbus_obj = self.connection.register_object(
            self.objpath,
            self._interface_info,
            self._cb_method,
            self._cb_property_read,
            self._cb_property_write,
        )
        return self

    @property
    def interface(self):
        return self._interface_info.name

    def remove(self):
        """
        Remove this object from the bus
        """
        self.connection.unregister_object(self._dbus_obj)

    def signal(self, name: str, arg: Optional[Any] = None, dest=None):
        """
        Emit the signal with the given name and args
        """
        if arg is not None:
            strarg = [f"{v}" if v != "" else "''" for v in arg]
        else:
            strarg = [""]

        logger.debug(f"Signal {name}({', '.join(strarg)})")
        self.connection.emit_signal(dest, self.objpath, self.interface, name, arg)

    def _cb_method(
        self, connection, sender, objpath, interface, methodname, args, invocation
    ):
        if interface != self.interface:
            return

        if self._interface_info.lookup_method(methodname) is None:
            logger.error(f"Non-existent method {self.interface}.{methodname}() called")
            return

        assert (
            self.wrapper is not None
        ), f"{self.interface}.{methodname}() called but no wrapper present"

        cb = getattr(self.wrapper, methodname, None)
        assert (
            cb is not None
        ), f"Method {self.interface}.{methodname}() is missing an implementation"

        strarg = [f"{v}" if v != "" else "''" for v in args]
        logger.debug(f"{methodname}({', '.join(strarg)})")

        # check if we have a "metainfo" arg and fill that with
        # meta information about this method call.
        # This way we can dynamically pass the sender to where we need it.
        func_args = inspect.getfullargspec(cb).args
        if "metainfo" in func_args:
            assert (
                func_args[-1] == "metainfo"
            ), "metainfo argument must be the last argument"
            ret = cb(*args, {"sender": sender})
        else:
            ret = cb(*args)

        if ret is not None:
            logger.debug(f"--> {methodname} reply {ret}")

            assert any([isinstance(ret, x) for x in [Response, Reply, RequestHandle]])

            if ret.fd_list:
                fd_list = Gio.UnixFDList.new()
                for f in ret.fd_list:
                    fd_list.append(f)
                return invocation.return_value_with_unix_fd_list(ret.value, fd_list)
            else:
                return invocation.return_value(ret.value)

    def _cb_property_read(self, connection, sender, objpath, interface, propname):
        if self.wrapper is not None:
            try:
                return getattr(self.wrapper, propname)
            except AttributeError:
                logger.error(
                    f"Property {self.interface}.{propname} is missing an implementation"
                )

    def _cb_property_write(self, connection, sender, objpath, interface, propname):
        pass

    @classmethod
    def create_from_interface_info(
        cls,
        connection,
        interface_info: Gio.DBusInterfaceInfo,
        objpath: str,
        wrapper: Any,
    ):
        return cls(
            connection=connection,
            objpath=objpath,
            interface_info=interface_info,
            wrapper=wrapper,
        )

    @classmethod
    def create_from_interface_name(
        cls, connection, interface: str, objpath: str, wrapper: Any
    ):
        iface = load_introspection(interface)
        return cls.create_from_interface_info(connection, iface, objpath, wrapper)


class PortalError(Exception):
    pass


@attr.s
class PortalFaker(GObject.Object):
    """
    The main context object. Use with

    >>> f = PortalFaker.create("org.foo.bar")
    >>> f.load_impl_portal("org.freedesktop.impl.portal.Whatever", options={})
    >>> f.run()

    and the rest happens automagically.
    """

    dbusserver: DBusServer = attr.ib()
    impl: Optional[Any] = attr.ib(init=False, default=None)

    def __attrs_pre_init__(self):
        super().__init__()

    def __attrs_post_init__(self):
        self.dbusserver.connect("bus-name-lost", lambda _: self.quit())

    def run(self) -> None:
        try:
            self.mainloop = GLib.MainLoop()
            self.mainloop.run()
        except KeyboardInterrupt:
            pass

    def schedule(self, callback: Callable, millis=0):
        # We have no use for timeout/idle functions that need to be rescheduled
        def wrap():
            callback()
            return False

        if millis == 0:
            GLib.idle_add(wrap)
        else:
            GLib.timeout_add(millis, wrap)

    def quit(self) -> None:
        GLib.idle_add(self.mainloop.quit)

    def load_portal(
        self,
        interface: str,
        config: Optional[Dict[str, Any]] = None,
    ) -> None:

        if "impl" in interface:
            print_portalfile_override(interface)

        def register_object(_):
            iname = interface.split(".")[-1]
            logger.debug(f"Registering portal for {interface}")

            if "impl" in interface:
                package = "portalfaker.impl.portal"
            else:
                package = "portalfaker.portal"

            module = importlib.import_module(f"{package}")
            klass = getattr(module, iname)

            dbus_obj = DBusObject.create_from_interface_name(
                connection=self.dbusserver.connection,
                interface=interface,
                objpath="/org/freedesktop/portal/desktop",
                wrapper=None,
            )

            klass.create(dbus_obj=dbus_obj, config=config or {})
            dbus_obj.register()
            return klass

        self.dbusserver.connect("bus-name-acquired", register_object)

    @classmethod
    def create(cls, busname: str) -> "PortalFaker":
        dbusserver = DBusServer.create(busname)
        instance = cls(dbusserver=dbusserver)
        instance.schedule(dbusserver.try_own_name)
        return instance

    @classmethod
    def list_portals(cls) -> List[str]:
        portals = []
        for file in Path("portalfaker").glob("*.py"):
            if file.name in ("__init__.py"):
                continue

            iname = file.stem
            modname = f"portalfaker.{iname}"
            try:
                module = importlib.import_module(modname)
            except ImportError:
                continue

            for m in inspect.getmembers(module, inspect.isclass):
                klass = m[1]
                if klass.__module__ != modname:
                    continue

                cname = klass.__name__
                if cname.startswith("Impl"):
                    portals.append(f"org.freedesktop.impl.portal.{cname[4:]}")
                elif cname.startswith("Portal"):
                    portals.append(f"org.freedesktop.portal.{cname[6:]}")

        return portals
