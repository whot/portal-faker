#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Any, Dict

from portalfaker import DBusObject, Response, Reply
from gi.repository import Gio, GLib, GObject

import portalfaker.inputcapture

import attr
import logging

logger = logging.getLogger("portalfaker.impl.portal")


@attr.s
class Session(GObject.Object):
    """
    Implementation of the org.freedesktop.impl.portal.Session DBus interface.
    See the portal documentation for details but basically:

    The xdg-desktop-portal calls a CreateSession DBus method with a session
    "session_handle_token".  From that we create an object path and create the
    Session DBus object. The Session handle (==objectpath) is returned to the
    caller in the Request's results dictionary.

    The caller *may* Close the session for any reason, or we may send out the
    Closed signal for any reason to interrupt.
    """

    handle: str = attr.ib()
    _dbus_obj: DBusObject = attr.ib()

    __gsignals__ = {
        "closed": (GObject.SignalFlags.RUN_FIRST, None, ()),
    }

    def __attrs_pre_init__(self):
        super().__init__()

    def close(self):
        """
        Entry point for the impl.portal to close the session. This does **not**
        remove the session.
        """
        self.signal("Closed")  # this is the dbus signal

    def Close(self) -> None:
        """
        DBus entry point when the session is closed by the xdg-desktop-portal.
        """
        self.emit("closed")  # this is the gobject signal

    def remove(self) -> None:
        self._dbus_obj.remove()

    @classmethod
    def create(cls, connection, objpath: str):
        dbus_obj = DBusObject.create_from_interface_name(
            connection=connection,
            interface="org.freedesktop.impl.portal.Session",
            objpath=objpath,
            wrapper=None,
        )
        c = cls(handle=objpath, dbus_obj=dbus_obj)
        dbus_obj.register()
        return c


@attr.s
class InputCapture:
    """
    Implementation of the org.freedesktop.impl.portal.InputCapture DBus
    interface. See the portal documentation for details.

    See the portalfaker.inputcapture module for the actual logic, this is
    merely the wrapper class to/fro the DBus interface.
    """

    connection: Gio.DBusConnection = attr.ib()
    _dbus_obj: DBusObject = attr.ib()
    config: Dict[str, Any] = attr.ib(factory=dict)
    sessions: Dict[str, Session] = attr.ib(factory=dict, init=False)

    @classmethod
    def create(cls, dbus_obj: DBusObject, config=None):
        instance = cls(
            connection=dbus_obj.connection, dbus_obj=dbus_obj, config=config or {}
        )
        dbus_obj.wrapper = instance
        return instance

    def CreateSession(
        self, handle, session_handle, app_id, parent_window, options
    ) -> Response:
        caps = options["capabilities"]

        dbus_session = Session.create(self.connection, session_handle)
        session = portalfaker.inputcapture.Session.create(
            self.connection, dbus_session, caps, config=self.config
        )
        self.sessions[session_handle] = session

        # Session signals
        def Disabled(session):
            self._dbus_obj.signal(
                "Disabled", GLib.Variant("(sa{sv})", (session.handle, {}))
            )

        def Activated(session, opts):
            options = {
                "activation_id": GLib.Variant("u", opts["activation-id"]),
                "cursor_position": GLib.Variant("(dd)", opts["cursor-position"]),
                "barrier_id": GLib.Variant("u", opts["barrier-id"]),
            }
            self._dbus_obj.signal(
                "Activated", GLib.Variant("(sa{sv})", (session.handle, options))
            )

        def Deactivated(session, opts):
            options = {
                "activation_id": GLib.Variant("u", opts["activation-id"]),
            }
            self._dbus_obj.signal(
                "Deactivated", GLib.Variant("(sa{sv})", (session.handle, options))
            )

        def ZonesChanged(session):
            self._dbus_obj.signal(
                "ZonesChanged", GLib.Variant("(sa{sv})", (session.handle, {}))
            )

        session.connect("disabled", Disabled)
        session.connect("activated", Activated)
        session.connect("deactivated", Deactivated)
        session.connect("zones-changed", ZonesChanged)

        results = {
            "capabilities": GLib.Variant.new_uint32(session.capabilities),
        }
        return Response(0, results)

    def ConnectToEIS(self, session_handle, app_id, options) -> GLib.Variant:
        session = self.sessions[session_handle]
        return Reply(
            value=GLib.Variant("(h)", (0,)), fd_list=[session.eis_socket.fileno()]
        )

    def GetZones(self, handle, session_handle, app_id, options) -> Response:
        session = self.sessions[session_handle]

        results = {
            "zone_set": GLib.Variant("u", session.zone_set),
            "zones": GLib.Variant(
                "a(uuii)",
                [
                    GLib.Variant("(uuii)", (z.w, z.h, z.x, z.y))
                    for z in session.zones
                    if z.id == session.zone_set
                ],
            ),
        }

        return Response(0, results)

    def SetPointerBarriers(
        self, handle, session_handle, app_id, options, barriers, zone_set
    ) -> Response:
        session = self.sessions[session_handle]

        bs = []
        failed = []
        for barrier in barriers:
            id = barrier["barrier_id"]
            position = barrier["position"]

            b = portalfaker.inputcapture.Barrier.create(id, position)
            if not b.validate() or session.zone_set != zone_set:
                failed.append(id)
            else:
                bs.append(b)

        if bs:
            session.set_barriers(bs)
            success = [b.id for b in session.barriers]
            failed.extend([b.id for b in bs if b.id not in success])
        response = 0
        results = {"failed_barriers": GLib.Variant("au", failed)}

        return Response(response, results)

    def Enable(self, session_handle, app_id, options) -> Response:
        session = self.sessions[session_handle]
        session.enable()
        return Response(0, {})

    def Disable(self, session_handle, app_id, options) -> Response:
        session = self.sessions[session_handle]
        session.disable()
        return Response(0, {})

    def Release(self, session_handle, app_id, options) -> Response:
        session = self.sessions[session_handle]
        session.release()
        return Response(0, {})
