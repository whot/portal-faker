#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Any, Dict, Generator, List, Tuple, Optional, Union

from gi.repository import GLib, GObject  # type: ignore

import portalfaker.impl.portal
import portalfaker.portal

from itertools import count, cycle, groupby

import attr
import click
import enum
import logging
import os
import socket

logger = logging.getLogger("portalfaker.InputCapture")


def next_serial(c=count()) -> int:
    return next(c)


class Capability(enum.IntEnum):
    KEYBOARD = 0x1
    POINTER = 0x2
    TOUCH = 0x4

    @classmethod
    def from_string(cls, name: str) -> "Capability":
        for c in cls:
            if c.name == name.upper():
                return c
        raise ValueError(f"Invalid capability {name}")


@attr.s
class Zone:
    x: int = attr.ib()
    y: int = attr.ib()
    w: int = attr.ib()
    h: int = attr.ib()
    id: Optional[int] = attr.ib()  # the zone-set id (if any)

    @classmethod
    def create(cls, w, h, x=0, y=0, id=None) -> "Zone":
        return cls(w=w, h=h, x=x, y=y, id=id)

    @classmethod
    def from_string(cls, str, id=None) -> List["Zone"]:
        zones = []
        for s in str.split(";"):
            if not s:
                continue
            dim, pos = s.split("@")
            x, y = map(int, pos.split(","))
            w, h = map(int, dim.split("x"))
            zones.append(cls.create(w=w, h=h, x=x, y=y, id=id))
        return zones

    @property
    def edges(self) -> List[Tuple[int, int, int, int]]:
        x, y, w, h = self.x, self.y, self.w, self.h
        return [
            (x, y, x + w, y),
            (x, y, x, y + h),
            (x + w, y, x + w, y + h),
            (x, y + h, x + w, y + h),
        ]


@attr.s
class Barrier:
    id: int = attr.ib()
    position: Tuple[int, int, int, int] = attr.ib()

    def validate(self):
        x1, y1, x2, y2 = self.position
        return (
            any([x1 == x2, y1 == y2])
            and any([x1 != x2, y1 != y2])
            and all([x1 <= x2, y1 <= y2])
        )

    @classmethod
    def create(cls, id: int, position: Tuple[int, int, int, int]):
        return cls(id=id, position=position)


class SessionState(enum.Enum):
    CREATED = enum.auto()
    ENABLED = enum.auto()
    ACTIVE = enum.auto()
    DISABLED = enum.auto()
    CLOSED = enum.auto()


@attr.s
class Session(GObject.Object):
    """
    One currently ongoing InputCapture session. This class implements the
    actual logic, where there is some.
    """

    _dbus_session: Union[
        portalfaker.portal.Session, portalfaker.impl.portal.Session
    ] = attr.ib()
    options: Dict[str, Any] = attr.ib()
    _zone_sets: List[List[Zone]] = attr.ib()
    zone_set: int = attr.ib(init=False, default=next_serial())
    _zone_sets_iterator: Generator = attr.ib(init=False)
    zones: List[Zone] = attr.ib(init=False)
    capabilities: int = attr.ib(default=0x7)

    state: SessionState = attr.ib(default=SessionState.CREATED, init=False)
    _barriers: List[Barrier] = attr.ib(factory=list, init=False)
    _eis_socket: Optional[socket.socket] = attr.ib(init=False, default=None)
    _socketpair: Optional[Tuple[socket.socket, socket.socket]] = attr.ib(
        init=False, default=None
    )
    _closed_sig: int = attr.ib(init=False)
    _active_serials: List[int] = attr.ib(factory=list, init=False)

    __gsignals__ = {
        "disabled": (GObject.SignalFlags.RUN_FIRST, None, ()),
        "activated": (GObject.SignalFlags.RUN_FIRST, None, (GObject.TYPE_PYOBJECT,)),
        "deactivated": (GObject.SignalFlags.RUN_FIRST, None, (GObject.TYPE_PYOBJECT,)),
        "zones-changed": (GObject.SignalFlags.RUN_FIRST, None, ()),
    }

    def __attrs_pre_init__(self):
        super().__init__()

    def __attrs_post_init__(self):
        self._closed_sig = self._dbus_session.connect(
            "closed", self._on_impl_session_closed
        )

    @property
    def handle(self):
        return self._dbus_session.handle

    @_zone_sets.default  # type: ignore
    def _zone_sets_default(self):
        zones = self.options.get("zones", ["1920x1080@0,0"])
        return [Zone.from_string(z) for z in zones]

    @_zone_sets_iterator.default  # type: ignore
    def _zone_sets_iterator_default(self):
        return cycle(self._zone_sets)

    @zones.default  # type: ignore
    def _zones_default(self):
        zones = next(self._zone_sets_iterator)
        for z in zones:
            z.id = self.zone_set
        return zones

    @property
    def eis_socket(self):
        if self._eis_socket is not None:
            return self._eis_socket

        libei_socket = os.getenv("LIBEI_SOCKET")
        if libei_socket:
            try:
                self._eis_socket = socket.socket(socket.AF_UNIX)
                self._eis_socket.connect(libei_socket)
            except Exception as e:
                logger.error(f"Failed to connect to LIBEI_SOCKET ({libei_socket}): {e}")
                self._eis_socket = None
        else:
            logger.warning("LIBEI_SOCKET not set")

        if self._eis_socket is None:
            logger.warning("Providing a dud socket")
            self._socketpair = socket.socketpair()
            self._eis_socket = self._socketpair[1]
        return self._eis_socket

    @property
    def barriers(self):
        return self._barriers

    def enable(self):
        if self.state not in [SessionState.CREATED, SessionState.DISABLED]:
            logger.error(f"Trying to enable a session in state {self.state}")
            return

        self.state = SessionState.ENABLED
        ms = self.options.get("disable-after", 0)
        if ms > 0:
            GLib.timeout_add(ms, self._disabled)

        ms = self.options.get("activate-after", 0)
        if ms > 0:
            GLib.timeout_add(ms, self._activated)

        ms = self.options.get("change-zones-after", 0)
        if ms > 0:
            GLib.timeout_add(ms, self._zones_changed)

    def disable(self):
        if self.state not in [
            SessionState.CREATED,
            SessionState.ENABLED,
            SessionState.ACTIVE,
        ]:
            logger.error(f"Trying to disable in a session in state {self.state}")
            return

        self.state = SessionState.DISABLED

    def _disabled(self):
        """
        Notify any callers that we are disabled.
        """
        if self.state not in [
            SessionState.ENABLED,
        ]:
            logger.error(
                f"Trying to signal disabled in a session in state {self.state}"
            )
            return

        self.state = SessionState.DISABLED
        self.emit("disabled")

    def _activated(self):
        """
        Notify any callers that we are active.
        """
        if self.state not in [
            SessionState.ENABLED,
        ]:
            logger.error(
                f"Trying to signal activated in a session in state {self.state}"
            )
            return

        self.state = SessionState.ACTIVE

        if not self.barriers:
            logger.warning("Unable to activate - no barriers")
            return

        # This is a debugging tool, so let's hardcode the behavior:
        # Always the last barrier that activates
        # EIS serial is always 1234, good enough to show up in debug logs
        # cursor-position is always last zone middle
        z = self.zones[-1]
        serial = next_serial()
        self._active_serials.append(serial)
        options = {
            "barrier-id": self.barriers[-1].id,
            "activation-id": serial,
            "cursor-position": (z.x + z.w / 2, z.y + z.h / 2),
        }

        self.emit("activated", options)

        ms = self.options.get("deactivate-after", 0)
        if ms > 0:
            GLib.timeout_add(ms, self._deactivated)

    def _deactivated(self):
        """
        Notify any callers that we are no longer active.
        """
        if self.state not in [
            SessionState.ACTIVE,
        ]:
            logger.error(
                f"Trying to signal deactivated in a session in state {self.state}"
            )
            return

        self.state = SessionState.ENABLED
        # See _activated()
        options = {
            "activation-id": self._active_serials.pop(0),
        }
        self.emit("deactivated", options)

    def _zones_changed(self):
        """
        Notify any callers that we changed our zones
        """
        if self.state in [SessionState.CLOSED]:
            logger.error(
                f"Trying to signal zones-changed in a session in state {self.state}"
            )
            return

        self.zones = next(self._zone_sets_iterator)
        self.zone_set = next_serial()
        for z in self.zones:
            z.id = self.zone_set
        self.emit("zones-changed")

    def release(self):
        if self.state not in [SessionState.ACTIVE]:
            logger.error(f"Trying to enable a session in state {self.state}")
            return

        self.state = SessionState.ENABLED

    def set_barriers(self, barriers: List[Barrier], zone_set: int):
        bs = []
        if self.zone_set == zone_set:
            # Collect all edges from all zones, any edge shared by zone will
            # occur twice. Thus we can allow any barrier that matches any
            # unique zone edge since that is an outside edge.
            # Doesn't cover screens of different sizes but is
            # good enough for debugging use-cases.
            edges = []
            for z in self.zones:
                edges += z.edges

            edges = sorted(edges)
            duplicates = [x for x, y in groupby(edges) if len(list(y)) > 1]

            for b in [barrier for barrier in barriers if barrier.validate()]:
                if b.position in edges and b.position not in duplicates:
                    bs.append(b)

        self._barriers = bs

    def _on_impl_session_closed(self):
        self.closed = True
        self._dbus_session.disconnect(self._closed_sig)
        self._dbus_session.remove()

    @classmethod
    def create(
        cls,
        connection,
        dbus_session: Union[
            portalfaker.portal.Session, portalfaker.impl.portal.Session
        ],
        capabilities: int,
        options: Optional[Dict[str, Any]] = None,
    ):
        options = options or {}
        zone_sets = options.get("zone-sets", [[Zone.create(x=0, y=0, w=1920, h=1080)]])
        return cls(
            dbus_session=dbus_session,
            capabilities=capabilities,
            options=options,
            zone_sets=zone_sets,
        )
