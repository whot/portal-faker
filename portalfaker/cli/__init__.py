#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Optional

from portalfaker import PortalFaker, PortalError
from portalfaker.cli.inputcapture import impl_inputcapture, portal_inputcapture

import click
import logging
import sys


def _init_logger(verbose: int) -> None:
    if verbose >= 1:
        lvl = logging.DEBUG
    else:
        lvl = logging.INFO

    logging.basicConfig(format="%(levelname).1s| %(name)s: %(message)s", level=lvl)


@click.group()
@click.option("--verbose", "-v", count=True, help="Enable debug logging")
@click.pass_context
def portal_faker(ctx, verbose: int):
    """
    portal-faker is a CLI utility that implements XDG desktop portals.

    This tool is primarily focused at debugging and testing libportal and
    xdg-desktop-portal implementations.

    Instantiation of on a portal is generally:

        $ portal-faker portal PortalName

    Instantiation of an impl.portal is generally:

        $ portal-faker impl PortalName

    """
    global logger

    _init_logger(verbose)
    logger = logging.getLogger("portalfaker")


@portal_faker.command(name="list-portals")
@click.pass_context
def portal_faker_list_portals(ctx):
    """
    List available portals
    """
    for p in PortalFaker.list_portals():
        click.secho(p)


@portal_faker.group(name="portal")
@click.option(
    "--busname",
    type=str,
    default="org.freedesktop.portal.Desktop",
    help="The portal bus name",
)
@click.pass_context
def portal_faker_portal(ctx, busname: str):
    """
    Fake an org.freedesktop.portal XDG Portal
    """
    try:
        ctx.obj = PortalFaker.create(busname=busname)
    except PortalError as e:
        click.secho(f"{e}", fg="red")
        sys.exit(1)


@portal_faker.group(name="impl")
@click.option(
    "--busname",
    type=str,
    help="The portal name",
    default="org.freedesktop.PortalFaker",
)
@click.pass_context
def portal_faker_impl(ctx, busname: str):
    """
    Fake an org.freedesktop.impl.portal XDG Portal
    """
    try:
        ctx.obj = PortalFaker.create(busname=busname)
    except PortalError as e:
        click.secho(f"{e}", fg="red")
        sys.exit(1)


portal_faker_impl.add_command(impl_inputcapture)
portal_faker_portal.add_command(portal_inputcapture)

if __name__ == "__main__":
    portal_faker()
