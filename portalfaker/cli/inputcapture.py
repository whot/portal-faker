#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from portalfaker.inputcapture import Zone, Capability

import click


@click.command(name="InputCapture", help="The impl.portal.InputCapture portal")
@click.option(
    "--disable-after",
    type=int,
    default=5000,
    help="Disable N milliseconds after Enable",
)
@click.option(
    "--activate-after",
    type=int,
    default=2000,
    help="Activate N milliseconds after Enable",
)
@click.option(
    "--deactivate-after",
    type=int,
    default=2000,
    help="Deactivate N milliseconds after Activated",
)
@click.option(
    "--change-zones-after",
    type=int,
    default=6000,
    help="Change Zones N milliseconds after GetZones. Cycles through the sets of Zones given in --zones",
)
@click.option(
    "--zones",
    multiple=True,
    type=str,
    default=["1920x1080@0,0"],
    help="A set of Zones in format WxH@x,y;WxH@x,y. Using multiple --zones specifies multiple sets that can be cycled through with --change-zone-after",
)
@click.option(
    "--version",
    type=int,
    default=1,
    help="The interface version of this portal",
)
@click.option(
    "--capabilities",
    type=str,
    default="pointer,keyboard,touch",
    help="A comma-separated string of supported capabilties, default: 'pointer,keyboard,touch'",
)
@click.pass_context
def impl_inputcapture(
    ctx,
    zones,
    disable_after,
    activate_after,
    deactivate_after,
    change_zones_after,
    version,
    capabilities,
):
    capabilities = sum([Capability.from_string(c) for c in capabilities.split(",")])
    zone_sets = [Zone.from_string(z) for z in zones]
    config = {
        "zone-sets": zone_sets,
        "disable-after": disable_after,
        "activate-after": activate_after,
        "deactivate-after": deactivate_after,
        "change-zones-after": change_zones_after,
        "version": version,
    }
    interface = "org.freedesktop.impl.portal.InputCapture"
    pp = ctx.obj
    pp.load_portal(interface, config)
    pp.run()


@click.command(name="InputCapture", help="The portal.InputCapture portal")
@click.option(
    "--disable-after",
    type=int,
    default=5000,
    help="Disable N milliseconds after Enable",
)
@click.option(
    "--activate-after",
    type=int,
    default=2000,
    help="Activate N milliseconds after Enable",
)
@click.option(
    "--deactivate-after",
    type=int,
    default=2000,
    help="Deactivate N milliseconds after Activated",
)
@click.option(
    "--change-zones-after",
    type=int,
    default=6000,
    help="Change Zones N milliseconds after GetZones. Cycles through the sets of Zones given in --zones",
)
@click.option(
    "--zones",
    multiple=True,
    type=str,
    default=["1920x1080@0,0"],
    help="A set of Zones in format WxH@x,y;WxH@x,y. Using multiple --zones specifies multiple sets that can be cycled through with --change-zone-after",
)
@click.pass_context
def portal_inputcapture(
    ctx, zones, disable_after, activate_after, deactivate_after, change_zones_after
):
    zone_sets = [Zone.from_string(z) for z in zones]
    config = {
        "zone-sets": zone_sets,
        "disable-after": disable_after,
        "activate-after": activate_after,
        "deactivate-after": deactivate_after,
        "change-zones-after": change_zones_after,
    }
    interface = "org.freedesktop.portal.InputCapture"
    pp = ctx.obj
    pp.load_portal(interface, config)
    pp.run()
